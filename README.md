cretinhex
=========



Présentation
------------

*CretinHex* est une application permettant de jouer au jeu *hex*, 
et répondant au cahier des charges fixé par la consigne de l'UE *Projet S4* (Univ tlse3 - L2 info 2016-2017).

Ce travail a été réalisé par François MAHÉ et Pierre POMERET-COQUOT

Le code est rangé dans ces quatre dossiers :

  - **c/** : coeur de calcul et les IA écrite en C
  - **java/** : application et les interfaces-utilisateur écrites en Java
  - **papiers/** : documents de conception, de spécification et de bilan écrits dans le cadre de l'UE
  - **parametres/** : quelques réglages communs à tout le projet (doxygen et makefiles)

Chaque section est détaillée dans le fichier *README.md* correspondant *(i.e. c/README.md, java/README.md, etc.)*.  

Architecture logicielle
-----------------------

Le logiciel est composé de paquetages Java, dont l'un *(noyau)* communique avec les bibliothèques C réalisées (JNI), comme illustré :

	#-----------------------------------------------------------------#
	| JAVA                                                            |
	|                         cretinplay (main)                       |   <- paquetage
	|                                |                                |
	|                                V                                |
	|                IHM (ligne de commande ou graphique)             |   <- paquetage
	|                                |                                |
	|                                V                                |
	|  noyau contenant les classes qui utilisent les biblis natives   |   <- paquetage
	|          |                     |           |          |         |
	#----------|---------------------|-----------|----------|---------#
		       |                     |           |          |
	   JNI     |                     |           |          |             <- Quatre insterfaces JNI
		       |                     |           |          | 
	#----------|---------------------|-----------|----------|---------#
	| C        V                     V           V          V         |
	|       Partie                  IA0         IA1        IA2        |   <- Quatre biblis
	|          |                                 |          |         |
	|          V                                 V          V         |
	|  Structures et fonctions en C pour la manipulation des données  |   <- Dont 3 utilisent le reste du C
	|                                                                 |
	#-----------------------------------------------------------------#


Compilation
-----------

Utilisez le makefile pour compiler l'application entière

	make

Et lancez l'application avec l'exécutable produit

	./cretinhex_thegame

Cet exécutable comprend quelques options :

	# Utiliser l'interface graphique par défaut
	./cretinhex_thegame
	
	# Utilisez une palette d'images et de couleurs particulière
	./cretinhex_thegame -ihm classic|rasta|abeille
	
	# Utilisez l'interface en ligne de commande (système unix, largeur du terminal)
	./cretinhex_thegame -ihm console [-sys=unix] [-l xxx]


Options du makefile
-------------------

Compile :

	# Tout
	make [all]
	
	# La partie C
	make c
	
	# La partie Java
	make java

Lance les séries de tests (un avant-goût du dossier c/tests/) :

	# Tous les tests
	make tests
	
	# Tous les tests en vérifiant les fuites mémoires
	make tests_memoire
	make memory_tests

Génère la documentation doxygen dans le dossier doc/ :

	# Tout
	make doc
	
	# La documentation de la partie C
	make cdoc
	
	# La documentation de la partie Java
	make javadoc

Crée le dossier client (sans les codes sources cachés) :

	make client

Un peu de nettoyage :

	# Nettoie les dossiers c/ et java/
	make [max]clean
	
	# Efface la doc
	make doc-clean
	
	# Grand nettoyage : efface tout ce qui est re-générable
	make maxmaxclean


Le makefile général, ainsi que tous les sous-makefiles (présents dans les sous-dossiers) lisent le fichier *parametres/make.txt*.
On y trouve les constantes nécessaires à la compilation.  
En cas de difficulté, les premières lignes, contenant des réglages personnels, peuvent être modifiées sans risque.

