/**
 * \file    parametres/doc.h
 * \brief   Commentaires pour Doxygen
 * \author  Pierre POMERET-COQUOT
 * \version 1.0
 * \date    Printemps 2017
 *
 * Ici sont les pages définitions de groupes du menu principal.
 *
 */
 
/**
 * \mainpage Cretinhex : le jeu
 * \anchor accueil_general
 *
 *
 * \par Introduction
 * <i>Cretinhex : le jeu</i> est un programme permettant de jouer au fameux jeu Hex.<br />
 * Il a été développé par F. Mahé et P. Pomeret-Coquot dans le cadre de l'UE <i>Projet S4</i> <em>(Univ Tlse3, L2 Info, Session 2016/2017)</em>.<br />
 * Comme le sujet le propose, ce programme est composé d'une bibliothèque native (écrite en C), nommée cretinhex, qui est utilisée par une application (écrite en Java), nommée CretinHex. 
 * Et il vit que cela était bon.
 * 
 * \par Documentation
 *   - \ref accueil_c "Bibliothèques C"      
 *   - \ref accueil_java "Application Java"
 *
 * \par Infomations supplémentaires
 *   - \ref RH "Présentation de l'équipe" 
 *   - \ref paperasse "Documents produits dans le cadre du projet" 
 * 
 *
 * \version 1.2
 * \date Printemps 2017
 * \author François Mahé
 * \author Pierre Pomeret-Coquot
 *
 *
 */

/**
 * \page RH Ressources humaines
 * \brief Informations quant aux développeurs
 * \section RH_infos Informations
 * La bibliothèque cretinhex à été développée dans le cadre du projet S4 (Univ tlse3, L2 Info, session 2016-2017
 *
 * Il s'agit d'un travail collectif effectué par les co-pharaons de Cretin Software Interactive, à savoir :
 *  - François MAHE
 *    - Numéro étudiant 21 50 62 01
 *    - Groupe 4.1
 *    - Mél : francois.mahe31@gmail.com
 *  - Pierre POMERET-COQUOT
 *    - Numéro étudiant 20 40 32 63
 *    - Groupe 4.2
 *    - Mél : pierre.pomeret@univ-tlse3.fr
 *
 */



/**
 * \defgroup parametres Paramètres
 * \brief    Paramètres pour make et doxygen
 * @{
 */

	/**
	 * \file    doxygen_all.txt
	 * \brief   Doxyfile général
	 */

	/**
	 * \file    doxygen_c.txt
	 * \brief   Doxyfile pour le code C
	 */

	/**
	 * \file    doxygen_java.txt
	 * \brief   Doxyfile pour le code Java
	 */

	/**
	 * \file    make.txt
	 * \brief   Paramètres communs à tous les Makefiles
	 */
/** @} */
/* Fin des paramètres */




/**
 * \defgroup paperasse Papiers
 * \brief Collection de papiers rendus pour le Projet S4
 * @{
 */

	/** 
	 * \file     Dossier_de_specification.pdf
	 * \brief    Cahier des charges
	 * 
	 * Ce cahier des charges contient la description du travail à faire, 
	 * sur lequel le client et l'équipe de développement se sont mis d'accord.
	 *
	 * \see <a href="../../papiers/Dossier_de_specification.pdf"> Document PDF</a>
	 */

	/** 
	 * \file Dossier_de_conception.pdf
	 * \brief Dossier de conception
	 *
	 * Le dossier de conception contient les diagrammes UML et les TADs sur lesquels nous nous appuyons pour développer ce logiciel.
	 * 
	 * \see <a href="../../papiers/Dossier_de_conception.pdf"> Document PDF</a>
	 */

	/** 
	 * \file Bilan.pdf
	 * \brief Bilan
	 *
	 * Le document de bilan contient le bilan, ainsi qu'une analyse de la complexité de certains modules
	 * 
	 * \see <a href="../../papiers/Bilan.pdf"> Document PDF</a>
	 */
/** @} */
/* Fin de la paperasse */

