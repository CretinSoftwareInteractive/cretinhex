/**
 * \package   ihm.rasta
 * \brief     Interface utilisateur  en mode graphique
 * \author    Pierre Pomeret-Coquot
 * \date      1er mai
 *
 * \par       Description
 *
 * La Fenetre est capable d'afficher quatres Cadre s (CadreAccueil, CadreNouvellePartie, CadreJeu et CadreHisto).  
 * Ces derniers sont composés de composants.Bloc s issus du paquetage ihm.rasta.composants
 *
 * La Fenetre dispose d'une apparences.Apparence (palette d'images et de couleurs pour l'affichage) qui peut être choisie au lancement 
 * (dans la commande exécutée), ou modifiée dans le menu *cretinhex -> Préférences -> Apparence* 
 *
 */
