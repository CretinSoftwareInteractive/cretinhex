/**
 * \file java/doc.h
 * \brief Commentaires pour Doxygen
 * \author Pierre Pomeret-Coquot
 * \version 1.0
 * \date 4 mars 2017
 *
 * Pages et groupes de la doc Java
 *
 */
 
/**
 * \mainpage Démonstrateur Java : cretinplay
 * \anchor accueil_java
 *
 * <a href="../../html/index.html">Retour à la page générale du projet</a>
 *
 * L'application java est composé de trois paquetages :
 *  - \ref cretinplay : *main*, interprétation de la commande passé et lancement d'une ihm
 *  - \ref ihm : deux ihm (interfaces-utilisateur) permettent d'interagir avec le noyau
 *       - ihm.console : interface en ligne de commande
 *       - ihm.rasta : interface graphique utilisant awt et swing 
 *  - \ref noyau : *cœur de calcul* de l'application, utilisant les bibliothèques natives (JNI)
 *
 */




	
