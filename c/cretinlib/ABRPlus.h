

# ifndef __CRETINLIB_ABRPLUS__
# define __CRETINLIB_ABRPLUS__

# include "ABR.h"
# include "LDC.h"


/**
 * \ingroup pack_cretinlib_abr
 * \brief   Donne les valeurs d'un ABR sous forme d'une LDC (les valeurs sont triées, croissantes)
 */
LDC ABR_lister(ABR abr);



# endif
