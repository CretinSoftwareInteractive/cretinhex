


/**
 * \defgroup pack_cretinlib cretinlib
 * \brief Manipulation de données (listes, arbres, etc.)
 * \author Pierre Pomeret-Coquot
 * \version 1.2
 * \date mars 2017
 *
 * \par Description
 * Structures de données manipulant des listes d'objets génériques
 *
 * \par Police de garantie des outils crétins
 * Bien qu'irréprochables, ces outils sont livrés sans garantie aucune. 
 * La responsabilité des développeurs ne saurait être engagé qu'à leur profit.
 *  
 */
