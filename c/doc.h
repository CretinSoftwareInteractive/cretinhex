/**
 * \file c/doc.h
 * \brief Commentaires pour Doxygen
 * \author Pierre POMERET-COQUOT
 * \version 1.0
 * \date 4 mars 2017
 *
 * Ici se trouvent les pages et définitions de groupes de la doc C
 *
 */
 
/**
 * \mainpage Bibliothèques c pour cretinhex
 * \anchor accueil_c
 *
 * <a href="../../html/index.html">Retour à la page générale du projet</a>
 *
 * Vous consultez la documentation de la **partie C**.
 * 
 *   - \subpage pack_cretinlib : Types génériques
 *   - \subpage pack_hex : Types spécifiques au jeu de Hex
 *   - \subpage pack_ia : Intelligences artificielles
 *   - \subpage pack_tests : Tests des modules
 *
 * Ce travail a été réalisé par François MAHÉ et Pierre POMERET-COQUOT
 *
 */


/**
 * \defgroup pack_ia Intelligences Artificielles
 * \brief    Regroupe les IA
 */


/**
 * \defgroup pack_tests Tests
 * \brief    Tests de tous les modules
 */



/**
 * \dir c/cretinlib 
 * \brief Gestion des données
 *
 * \dir c/hex
 * \brief Gestion du jeu Hex
 *
 * \dir c/ia_randombot
 * \brief Joue des coups valides aléatoires
 *
 * \dir c/tests
 * \ingroup pack_tests
 * \brief Programmes de test des modules
 */
